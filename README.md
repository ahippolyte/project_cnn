# CNN Filters

Here is a tool developped to visualize convolutional filters generated from a convolutional neuron network. 

## Build requirments package

```sh
cd program
```

```sh
sh ./scripts/build.sh
```

## Install requirments package

```sh
cd program
```

```sh
sh ./scripts/build.sh
```

## Run program

### With requirments installation (*Not necessiting build & installation steps*)

```sh
cd program
```

```sh
sh ./scripts/run.sh <-i|--install> ...
```

**Example :**
```sh
sh ./scripts/run.sh <-i|--install> --data 2
```

### Without requirments installation (When requirments installation have been already complete)

```sh
cd program
```

```sh
sh ./scripts/run.sh ...
```

**Example :**
```sh
sh ./scripts/run.sh --help
```

## Commands

Command list :
- **--data (-d) :**
    - `empty` argument : List availables dataset.
    - `int` argument : Execute program on selected dataset.
    - `YourOwnCSVPath` argument : Import & execute program on selected csv.

- **--hist :**
    - `empty` argument : List history of previous executions.
    - `int` argument : Rebuild saved model.


- **--help (-h) :**
    - `empty` argument: List commands

## Configurations

- `program/content` folder contain preloaded and imported datasets.
- `program/history` folder contain saved model from executed datasets.

**If you want to change folders, please edit `program/config/config.json` file.**