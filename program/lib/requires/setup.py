from setuptools import setup, find_packages

setup(
    name='requires',
    version='1.0',
    author='CNN_Project_Team',
    description='Package description',
    packages=find_packages(),
    install_requires=[
        'tensorflow',
        'tensorflow_datasets',
        'pandas',
        'pygame',
        'scikit-learn',
        'matplotlib',
        'pillow'
    ]
)
