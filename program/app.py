from classes.builders.CommandBuilder import CommandBuilder
from classes.routers.router import Router
import sys, getopt


def main(argv):
    commandList = CommandBuilder().build()
    router = Router(commandList)
    if(len(argv) > 1):
        router.route(argv[0], argv[1])
    else:
        if(len(argv) == 1):
            router.route(argv[0])
        else:
            print("\33[0;31mAucune commande n'a été entrée.\33[0;0m")
            router.route("--help")

if __name__ == "__main__":
    main(sys.argv[1:])