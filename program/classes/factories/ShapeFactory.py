from classes.shapes.Circle import Circle
from classes.shapes.Square import Square
from classes.shapes.Triangle import Triangle 

class ShapeFactory:
    def createSquare(self, size, x, y, number):
        s = Square(x, y, size, number)
        return s

    def createCircle(self, size, x, y, number):
        c = Circle(x, y, size, number)
        return c
    
    def createTriangle(self, size, x, y, number):
        t = Triangle(x, y, size, number)
        return t
