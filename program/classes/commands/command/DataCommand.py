from classes.commands.command.Command import Command
from classes.builders.DatasetsViewBuilder import DatasetViewBuilder
from classes.proxies.SymbolicProxy import SymbolicProxy
from classes.ui.DatasetGeneratorUI import DatasetGeneratorUI
from classes.utilities.DatasetGenerator import DatasetGenerator
from classes.utilities.TensorflowLoader import TensorflowLoader
from classes.utilities.ModelMaker import ModelMaker
from classes.ui.ModelMakerUI import ModelMakerUI
from classes.ui.DataTrainingUI import DataTrainingUI
from classes.utilities.DataTraining import DataTraining
from classes.utilities.ResultDisplay import ResultDisplay
from classes.utilities.FileExplorer import saveModel
from classes.utilities.ConfigNavigator import ConfigNavigator
from classes.utilities.CsvReader import CsvReader
import matplotlib.pyplot as plt

class DataCommand(Command):
    id = None
    datasets = None
    tf_datasets = ["dsprites", "mnist", "fashion_mnist"]

    def __init__(self, id = None):

        self.datasets = DatasetViewBuilder().build()
        if(id == None):
            super().__init__("Liste les datasets disponibles (y compris la génération de dataset artificiel)", "--data", "-d")
        else:
            super().__init__("Execute le programme sur un dataset", "--data <id|path>", "-d <id|path>")
            self.id = id

    def print(self):
        super().print()
        if(self.id == None):
            temp = DataCommand(0)
            temp.print()

    def run(self):
        images = None
        labels = None
        dgUI = DatasetGeneratorUI()
        if(self.id == None):
            # list datasets
            dgUI.printDatasets(self.datasets)
        elif self.id == "af":
            # dataset is artificial
            circles, squares, triangles, empties, number, size, save, folder = dgUI.start()
            dg = DatasetGenerator(circles, squares, triangles, empties, number, size, save, folder)
            images, labels = dg.generate()
        try : 
            self.id = int(self.id)
            if(self.id < len(self.datasets) and self.id >= 0):
                # dataset is Proxy, it can be csv path or tensorflow dataset id
                dataset = self.datasets[self.id-1]
                if(dataset.get_type() != "tensorflow"):
                    cr = CsvReader(dataset.get_item())
                    images, labels = cr.read()
                    pass
                else:
                    tf  = TensorflowLoader(dataset.get_item())
                    images, labels = tf.start()

                # TODO : call DataTraining with argument dataset
            else :
                print("Dataset incorrect !")
        except:
            path = self.id
            if path != None:
                # check if path is a path and csv file
                if(path.endswith(".csv")):
                    proxy = self.createSymLink(path)
                    dataset = proxy.get_item()
                    cr = CsvReader(dataset)
                    images, labels = cr.read()
                    # TODO : call DataTraining with argument dataset
                    pass

        if images is not None :
            mmUI = ModelMakerUI(images)
            layers, fnumber, fsize = mmUI.start()
            mm = ModelMaker(layers, fnumber, fsize, images, labels)
            model = mm.make_model()
            model_name = mmUI.askName()
            dtUI = DataTrainingUI()
            epochs, batch = dtUI.start()
            dt = DataTraining(images, labels, model, epochs, batch)
            trained_model = dt.start()
            saveModel(ConfigNavigator.find("folders/history"), trained_model, model_name)
            rd = ResultDisplay(trained_model)
            rd.start()
        else :
            ask = input("Selectionnez un dataset : ")
            dc = DataCommand(ask)
            dc.run()            

    def set_id(self, id):
        self.id = id

    def createSymLink(self, path):
        # get file name
        filename = path.split("/")[-1]
        proxy = SymbolicProxy(filename, path)
        proxy.save()
        self.datasets.append(proxy)
        return proxy