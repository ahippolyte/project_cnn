from classes.commands.command.Command import Command
from classes.ui.HelperUI import HelperUI
class HelperCommand(Command):
    def __init__(self):
        super().__init__("Liste les commandes disponibles", "--help", "-h")

    def run(self, commandList):
        HelperUI(commandList).start()