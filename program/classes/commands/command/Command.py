class Command:
    description = None
    command = None
    shortcut = None

    def __init__(self, description, command, shortcut):
        if(not description or not command):
            raise ValueError("Command attributes cannot be empty")
        self.description = description
        self.command = command
        self.shortcut = shortcut
    
    def get_description(self):
        return self.description
    
    def get_command(self):
        return self.command
    
    def get_shortcut(self):
        return self.shortcut
    
    def print(self):
        if(self.shortcut):
            print("\033[0;35m" + self.command + "\033[0m ou \033[0;35m" + self.shortcut + "\033[0m" + " : " + self.description)
        else:
            print("\033[0;35m" + self.command + "\033[0m" + " : " + self.description)

    def run():
        pass