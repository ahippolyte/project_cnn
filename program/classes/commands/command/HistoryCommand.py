from classes.commands.command.Command import Command
from classes.ui.HistoryUI import HistoryUI
from classes.utilities.FileExplorer import getFileContent

class HistoryCommand(Command):
    id = None

    def __init__(self, id = None):
        if(id == None):
            super().__init__("Liste l'historique des executions", "--hist", None)
        else:
            super().__init__("Affiche le résultat de l'execution selectionnée", "--hist <id>", None)
            self.id = id
    
    def set_id(self, id):
        try :
            self.id = int(id)
        except ValueError:
            self.id = None


    def print(self):
        super().print()
        if(self.id == None):
            temp = HistoryCommand(0);
            temp.print()

    def run(self):
            if(self.id == None):
                HistoryUI().printHistory()
                HistoryUI().modelSelection()
            else:
                HistoryUI().modelSelection(self.id)