class CommandList:
    commands = None

    def __init__(self):
        self.commands = []

    def add(self, command):
        self.commands.append(command)

    def get_commands(self):
        return self.commands

    def remove(self, command):
        self.commands.remove(command)

    def get_command(self, command):
        for c in self.commands:
            if c.get_command() == command or c.get_shortcut() == command:
                return c
        return None
