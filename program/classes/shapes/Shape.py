from abc import ABC, abstractmethod

class Shape():
    x,y,size,count = 0,0,0,0 
    def __init__(self, x, y, size, count):
        if size < 0:
            raise ValueError("Size must be greater than 0")
        if count < 0:
            raise ValueError("Count must be greater than 0")
        self.x = x
        self.y = y
        self.size = size
        self.count = count
        
    
    def get_x(self):
        return self.x
    
    def get_y(self):
        return self.y

    def get_size(self):
        return self.size
    
    def get_count(self):
        return self.count
    
    def draw_and_save(self, surface, folder):
        pass
    