from classes.shapes.Shape import Shape
import pygame as pg

class Circle(Shape):
    def __init__(self, x, y, size, count):

        super().__init__(x, y, size, count)

    def draw_and_save(self, surface, folder):
        pg.draw.circle(surface, (255, 255, 255), (self.x, self.y), self.size, 0)
        pg.image.save(surface, folder + "/circle" + str(self.count) + ".png")