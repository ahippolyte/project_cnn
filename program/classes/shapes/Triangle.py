from classes.shapes.Shape import Shape
import pygame as pg

class Triangle(Shape):
    def __init__(self, x, y, size, count):
        super().__init__(x, y, size, count)

    def draw_and_save(self, surface, folder):
        pg.draw.polygon(surface, (255, 255, 255), ((self.x, self.y), (self.x + self.size, self.y), (self.x + self.size/2, self.y + self.size)), 0)
        pg.image.save(surface, folder + "/triangle" + str(self.count) + ".png")
