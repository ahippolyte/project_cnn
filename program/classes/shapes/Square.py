from classes.shapes.Shape import Shape
import pygame as pg

class Square(Shape):
    def __init__(self, x, y, size, count):
        super().__init__(x, y, size, count)

    def draw_and_save(self, surface, folder):
        pg.draw.rect(surface, (255, 255, 255), (self.x, self.y, self.size, self.size), 0)
        pg.image.save(surface, folder + "/square" + str(self.count) + ".png")