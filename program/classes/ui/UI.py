

class UI:
    def start():
        pass

    def askDefaultFalse(self, answer):
        try:
            ask = int(self.parseInputResponse(input(answer + " (yes/\033[0;32mNo\033[0;0m) ")))
            return ask != 1
        except ValueError:
            return False

    def askDefaultTrue(self, answer):
        try:
            ask = int(self.parseInputResponse(input(answer + " (\033[0;32mYes\033[0;0m/no) ")))
            return ask == 1
        except ValueError:
            return True

    def parseInputResponse(self, input):
        input = input.lower()
        if input == "yes" or input == "y":
            return 1
        elif input == "no" or input == "n":
            return 0
        else:
            return input
