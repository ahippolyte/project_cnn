from classes.ui.UI import UI
from classes.utilities.FileExplorer import getFolderContent
from classes.utilities.FileExplorer import loadModel
from classes.utilities.ResultDisplay import ResultDisplay
from classes.utilities.ConfigNavigator import ConfigNavigator

class HistoryUI(UI):
    modelNameList = None

    def __init__(self):
        self.modelNameList = getFolderContent(ConfigNavigator.find("folders/history"))

    #Affiche la liste des models présent dans le fichier History/
    def printHistory(self):
        print("Aucune sélection ou une sélection invalide a été faite")
        print("Liste des modèles disponibles dans l'historique : ")
        for i in range(len(self.modelNameList)):
            print(str(i) + ": " + self.modelNameList[i])

    #Retourne le model selectionné parmis ceux présent dans le fichier history
    def modelSelection(self, id = None):

        if id != None and id >= len(self.modelNameList):
            id = None
            self.printHistory()

        while id == None:
            user_input = input("Entrez le \033[0;32mnuméro du model\033[0;0m que vous avez choisi : \n")
            if user_input != "":
                try:
                    id = int(user_input)
                    if id >= len(self.modelNameList):
                        id = None
                        self.printHistory()
                except ValueError:
                    print("\033[0;31mInvalid integer '%s', try again" % (user_input,) + "\033[0;0m")
                    self.printHistory()
            else:
                print("\033[0;31mLe numéro ne peut pas être vide !\033[0;0m")
                self.printHistory()
        model = loadModel(ConfigNavigator.find("folders/history"), self.modelNameList[id])
        display = ResultDisplay(model)
        display.display_filters()