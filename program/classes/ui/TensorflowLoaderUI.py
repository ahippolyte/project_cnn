from classes.ui.UI import UI

class TensorflowLoaderUI(UI):
    def __init__(self):
        pass

    def start():
        pass
    
    def choose_label(self, sample):
        i = 0
        print("Selectionnez le label de classification pour ce dataset :")
        for label in sample :
            print("\033[0;31m" , i, "\033[0;0m) ", label)
            i += 1
        choosen_label = input("Choisissez un label : ")
        try :
            choosen_label = int(choosen_label)
            if choosen_label < 1 or choosen_label >= len(sample):
                print("\033[0;31mChoix invalide !\033[0;0m")
                self.choose_label(sample)
                return
            else:
                choosen_label = list(sample.keys())[choosen_label]
                print("Chargement du set ", choosen_label + "... (cela peut prendre un certain temps)")    
        except ValueError:
            print("\033[0;31mChoix invalide !\033[0;0m")
            self.choose_label(sample)
            return
        return choosen_label
    
    def ask_part_of_dataset(self):
        partof = input("Choisissez une partie du dataset à charger (en %) : ")
        try :
            partof = int(partof)
            if partof < 1 or partof > 100:
                print("\033[0;31mLe pourcentage doit être compris entre 1 et 100.\033[0;0m")
                self.ask_part_of_dataset()
                return
        except ValueError:
            print("\033[0;31mChoix invalide !\033[0;0m")
            self.ask_part_of_dataset()
            return
        return partof