from classes.ui.UI import UI

class HelperUI(UI):
    commandList = None

    def __init__(self, commandList):
        self.commandList = commandList

    def start(self):
        print("Liste des commandes :")
        for command in self.commandList.get_commands():
            command.print()