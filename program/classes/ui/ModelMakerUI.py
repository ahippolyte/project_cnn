import re, random
from classes.ui.UI import UI

class ModelMakerUI(UI):
    images = None
    shape = None

    def __init__(self, images):
        self.images = images
        self.shape = images.shape[1:][0]
        pass

    def start(self):
        returns = []
        choose = super().askDefaultTrue("Voulez-vous utiliser les paramètres par défaut ? (3 layers, (32, 32, 64) filters, (" + str(max(self.shape//4, 5)) + "x" + str(max(self.shape//4, 5)) + "," + str(max(self.shape//8, 3)) + "x" + str(max(self.shape//8, 3)) + "," + str(max(self.shape//16, 3)) + "x" + str(max(self.shape//16, 3)) + ") size)")

        if choose:
            returns.append(3)
            returns.append([32, 32, 64])
            returns.append([max(self.shape//4, 5), max(self.shape//8, 3), max(self.shape//16, 3)])
        else:
            returns.append(self.askLayers())
            returns.append(self.askNumbers(returns[0]))
            returns.append(self.askSizes(returns[0],self.images.shape[1:]))
        return returns

    def askLayers(self):
        layers = 0
        try :
            ask = int(input("Combien de\033[0;34m layers\033[0;0m voulez vous (default: 3) ? "))
            if ask == "":
                ask = 3
        except ValueError:
            return self.askLayers()
        if ask > 0:
            layers = ask
        else :
            print("\033[0;31mLe nombre de layers doit être supérieur à 0.\033[0;0m")
            return self.askLayers()
        return layers
    
    def askNumbers(self, layers):
        numbers = []
        for i in range(layers):
            try :
                ask = int(input("Combien de\033[0;34m filtres\033[0;0m voulez-vous pour vos layers ? " + str(i) +  "?"))
            except ValueError:
                return self.askNumbers(layers)
            if ask > 0:
                numbers.append(ask)
            else :
                print("\033[0;31mLe nombre de filtres doit être supérieur à 0.\033[0;0m")
                return self.askNumbers(layers)
        return numbers
    
    def askSizes(self, layers, input_shape):
        sizes = []
        for i in range(layers):
            try :
                ask = int(input("Quelle taille voulez-vous pour le\033[0;34m layers " +  str(i) + "\033[0;0m ? (max: " + str(input_shape[0]//pow(2,i)) + "x" + str(input_shape[1]//pow(2,i)) + ")"))
            except ValueError:
                print("\033[0;31mChoix invalide !\033[0;0m")
                return self.askSizes(layers, input_shape)
            if ask > 0:
                sizes.append(ask)
            else :
                print("\033[0;31mLa taille doit être supérieure à 0.\033[0;0m")
                return self.askSizes(layers, input_shape)
        return sizes
        
    def askName(self):
        name = None
        while(name == None):
            # generate a number between 0 and 9999
            generated_int = random.randint(0, 9999)
            
            name = input("Quel nom souhaitez-vous donner au \033[0;32mmodel\033[0;0m ? (default: generated_dataset_"+str(generated_int)+") ")
            if not re.search(r'^[^\\/?%*:|"<>\.]+$', name) or name == "":
                name = "generated_dataset" + str(generated_int)
        return name
        
