from classes.ui.UI import UI

class DataTrainingUI(UI): 

    def __init__(self) : 
        pass

    def start(self) :
        returns = []
        returns.append(self.ask_epochs())
        returns.append(self.ask_batchsize())
        return returns

    def ask_epochs(self):
        epochs = input ("Choisissez le nombre d'epochs : (default: 5)")

        try:
            if epochs == "":
                epochs = 5
                return epochs
            else:
                epochs = int (epochs)
                if epochs < 1 :
                    print ("Vous devez choisir au moins 1 epoch.")
                    self.ask_epochs()
                    return
        except ValueError:
            print ("\033[0;31mChoix invalide !\033[0;0m")
            self.ask_epochs()
            return

        return epochs
    
    def ask_batchsize(self):
        batchsize = input("Choisissez la taille du batch : (default is 32)")

        try :
            if batchsize == "" :
                batchsize = 32
                return batchsize
            else :
                batchsize = int (batchsize)
                if batchsize < 1 :
                    print ("La taille du batch doit être supérieure à 0.")
                    self.ask_batchsize ()
                    return
        except ValueError :
            print ("\033[0;31mChoix invalide !\033[0;0m")
            self.ask_batchsize ()
            return

        return batchsize
