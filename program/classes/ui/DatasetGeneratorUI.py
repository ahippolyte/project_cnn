from classes.ui.UI import UI

class DatasetGeneratorUI(UI):
    
    def __init__(self):
        super().__init__()

    def start(self):
        returns = []
        returns.append(super().askDefaultTrue("Voulez-vous générer des \033[0;32mcercles\033[0;0m ?"))
        returns.append(super().askDefaultTrue("Voulez-vous générer des \033[0;32mcarrés\033[0;0m ?"))
        returns.append(super().askDefaultTrue("Voulez-vous générer des \033[0;32mtriangles\033[0;0m ?"))
        returns.append(super().askDefaultFalse("Voulez-vous générer des \033[0;32mimages vide\033[0;0m ?"))
        if returns[0] == False and returns[1] == False and returns[2] == False and returns[3] == False:
            print("Aucune forme sélectionnée, seul les formes de type cercle seront générées.")
            returns[0] = True
        returns.append(self.askNumber())
        returns.append(self.askSize())
        returns.append(super().askDefaultFalse("Souhaitez-vous sauvegarder le dataset ?"))
        returns.append(self.askFolder(returns[6]))
        return returns
    
    def printDatasets(self, datasets):
        print("Liste des datasets :")
        i = 0
        for dataset in datasets:
            i += 1
            print("\033[0;33m" + str(i) + "\033[0;0m) " + dataset.get_name())
        print("\033[0;33maf\033[0;0m) Générer un dataset artificiel")
    
    def askNumber(self):
        value = 100
        while True:
            user_input = input("Combien d'images souhaitez-vous générer ? (default: 100) ")
            if user_input != "":
                try:
                    value = int(user_input)
                except ValueError:
                    print("\033[0;31mInvalid integer '%s', try again" % (user_input,) + "\033[0;0m")
                else:
                    break
            else:
                break

        return value
    
    def askSize(self):
        value = 256
        while True:
            user_input = input("Quelle taille souhaitez-vous que les images aient ? (default: 256) ")
            if user_input != "":
                try:
                    value = int(user_input)
                except ValueError:
                    print("\033[0;31mInvalid integer '%s', try again" % (user_input,) + "\033[0;0m")
                else:
                    break
            else:
                break

        return value
    
    def askFolder(self, save):
        if not save: 
            return "auto_generated_dataset"
        folder = input("Entrez le nom chemin vers le dossier où vous souhaitez sauvegarder le dataset : (default: auto_generated_dataset) ")
        if folder == "":
           folder = 'auto_generated_dataset'
        return folder
    
    def askSave(self):
        ask = int(super().parseInputResponse(input("Voulez-vous sauvegarder le dataset ? (yes/No) ")))
        return ask == 1
