class Router:
    commandList = None

    def __init__(self, commandList):
        self.commandList = commandList

    def route(self, arg, id = None):
        if(self.commandList != None):
            command = self.commandList.get_command(arg)
            if(command != None):
                if(command.get_command() == "--help"):
                    command.run(self.commandList)
                else:
                    if(id != None):
                        command.set_id(id)
                    command.run()
            else:
                self.commandList.get_command("--help").run(self.commandList)
