from classes.commands.CommandList import CommandList
from classes.commands.command import DataCommand, HistoryCommand, HelperCommand
from classes.builders.Builder import Builder
class CommandBuilder(Builder):
    commandList = None

    def __init__(self):
        self.commandList = CommandList()

    def build(self):
        CommandList.add(self.commandList, self.buildHelperCommand())
        CommandList.add(self.commandList, self.buildDataListCommand())
        CommandList.add(self.commandList, self.buildHistoryCommand())
        return self.commandList

    def buildHelperCommand(self):
        return HelperCommand.HelperCommand()

    def buildHistoryCommand(self):
        return HistoryCommand.HistoryCommand()
    
    def buildDataListCommand(self):
        return DataCommand.DataCommand()
