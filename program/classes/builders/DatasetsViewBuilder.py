from classes.builders.Builder import Builder
from classes.proxies.TensorflowProxy import TensorflowProxy
from classes.proxies.FileProxy import FileProxy
from classes.proxies.SymbolicProxy import SymbolicProxy
from classes.utilities.FileExplorer import getFolderContent, getFileContent
from classes.utilities.ConfigNavigator import ConfigNavigator
class DatasetViewBuilder(Builder):
    def __init__(self):
        super().__init__()


    def build(self):
        return self.buildTensorDataset() + self.buildContentFolderDatasets()

    def buildSymbolicProxy(self, name, item):
        proxy = SymbolicProxy(name, item)
        proxy.save()
        return proxy

    def buildContentFolderDatasets(self):
        datasetList = []
        content_path = ConfigNavigator.find("folders/content")
        folderContent = getFolderContent(content_path)
        for i in range(len(folderContent)):
            if folderContent[i].endswith(".proxy"):
                path = getFileContent(content_path + folderContent[i])
                # check if the file exists
                if path != None:
                    datasetList.append(SymbolicProxy(folderContent[i][:-6], path))
            else:
                if folderContent[i].endswith(".csv"):
                    datasetList.append(FileProxy(folderContent[i][:-4], content_path + folderContent[i]))
        return datasetList

    def buildTensorDataset(self):
        return [
            TensorflowProxy("DS Sprite", "dsprites"),
            TensorflowProxy("MNIST", "mnist"),
            TensorflowProxy("Fashion MNIST", "fashion_mnist")
        ]