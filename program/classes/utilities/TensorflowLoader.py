from tensorflow import keras
import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np
import matplotlib.pyplot as plt

from classes.ui.TensorflowLoaderUI import TensorflowLoaderUI

class TensorflowLoader:
    list_of_sets = []
    images, labels = [], []
    data = None
    set_to_load = None
    ui = None

    def __init__(self, set_to_load):
        self.set_to_load = set_to_load
        self.ui = TensorflowLoaderUI()
    
    def start(self):
        self.loadSet(self.set_to_load)
        return self.images, self.labels

    def loadSet(self, set):

        if self.data == None:
            try : 
                print("Loading set : ", set)
                self.data, info = tfds.load(set, split='train', shuffle_files=True, with_info=True)
            except:
                raise ValueError("\033[0;31mLe dataset n'est pas valide !\033[0;0m")
            
        partof = self.ui.ask_part_of_dataset()
        data_subset = self.data.take(int((partof/100) * tf.data.experimental.cardinality(self.data).numpy()))
        self.data=data_subset
        # print("Type  of dataset : ", type_of_dataset)
        choosen_label = None

        for sample in self.data :
            if len(sample) != 2:
                if choosen_label == None:
                    choosen_label = self.ui.choose_label(sample)   
                for sample in self.data :
                    self.images.append(sample[list(sample.keys())[0]])
                    self.labels.append(sample[choosen_label])
            else :
                self.images.append(sample[list(sample.keys())[0]])
                self.labels.append(sample[list(sample.keys())[1]])
        self.images = np.array(self.images)
        self.labels = np.array(self.labels)
        print("Set loaded")
