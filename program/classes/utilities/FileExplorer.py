from pathlib import Path
import keras
import os
import csv
import numpy as np
from PIL import Image
from classes.utilities.ConfigNavigator import ConfigNavigator


def getFolderContent(path): 
    dir_path = Path(path)
    try :
        file_list = [entry.name for entry in dir_path.iterdir()]
        return file_list
    except FileNotFoundError:
        os.mkdir(path)
        return []

def getFileContent(path):
    try:
        content = Path(path).read_text()
        return content
    except FileNotFoundError:
        raise FileNotFoundError(f"File {path} not found.")
    

def saveModel(path, model, name):
    try:
        model.save(path + name)
        print("Model " + name + "saved in file : " + path)
        return
    except ValueError:
        print("The model could not be saved")
        return

def loadModel(path, name):
    try:
        modelLoaded = keras.models.load_model(path + name)
        return modelLoaded
    except ValueError:
        print("The model could not be load")
        return
    
def create_csv(myFileList, labels, csv_name):
        i = 0
        with Image.open(myFileList[0]) as img:
            width, height = img.size

        with open(ConfigNavigator.find("folders/content") + csv_name, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(["p" + str(i) for i in range(width*height)] + ["label"])

        for file in myFileList:
            img_file = Image.open(file)
            # get original image parameters...
            width, height = img_file.size
            # Make image Greyscale
            img_grey = img_file.convert('L')
            # Save Greyscale values
            value = np.asarray(img_grey.getdata(), dtype=np.int).reshape((width, height))
            value = value.flatten()
            value = np.append(value,labels[i])
            i +=1
            # TODO : Use FileExplorer class
            with open(ConfigNavigator.find("folders/content") + csv_name, 'a') as f:
                writer = csv.writer(f)
                writer.writerow(value)


