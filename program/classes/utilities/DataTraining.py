from keras.utils import to_categorical
from sklearn.model_selection import train_test_split

class DataTraining :
    images = None
    labels = None
    model = None
    epochs = None
    batchsize = None
    train_images = None
    train_labels = None
    test_images = None
    test_labels = None

    def __init__(self, images, labels, model, epochs, batchsize):
        model.summary()
        self.images = images
        self.labels = labels
        self.model = model
        self.epochs = epochs
        self.batchsize = batchsize

    def start(self):
        self.normalize_data()
        self.split_data()
        return self.train()

    def normalize_data(self):
        self.images = self.images / 255.0
        self.labels = to_categorical(self.labels)

    def split_data(self):
        self.train_images, self.test_images, self.train_labels, self.test_labels = train_test_split(self.images, self.labels, test_size=0.2, random_state=42)

    def train(self):
        self.model.fit(self.images, self.labels, epochs=self.epochs, batch_size=self.batchsize,verbose=1, validation_data=(self.test_images, self.test_labels))
        return self.model
