from matplotlib import pyplot as plt

class ResultDisplay :
    model = None

    def __init__(self, model):
        self.model = model
        model.summary()

    def start(self):
        self.display_filters()

    def display_filters(self):
        conv_layers = [layer for layer in self.model.layers if 'conv' in layer.name]
        conv_weights = [layer.get_weights()[0] for layer in conv_layers]
        #display all filters from each layer in separate plots
        for i in range(len(conv_weights)):
            filters = conv_weights[i]
            print("Layer ", i, " has ", filters.shape[3], " filters")
            for j in range(filters.shape[3]):
                plt.subplot(filters.shape[3]//8+1, 8, j+1)
                plt.imshow(filters[:,:,0,j], cmap='gray')
                plt.axis('off')
            plt.show()
