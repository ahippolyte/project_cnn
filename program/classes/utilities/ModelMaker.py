import keras
from keras.models import Sequential,Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import LeakyReLU
from tensorflow.keras import datasets, layers, models
import numpy as np

class ModelMaker:
    nb_layers = None
    input_shape = None
    filters_size = None
    filters_number = None
    images = None
    labels = None

    def __init__(self, layers, filters_number, filters_size, images, labels):
        self.nb_layers = layers
        self.filters_size = filters_size
        self.filters_number = filters_number
        self.images = images
        self.labels = labels
        self.input_shape = (self.images.shape[1], self.images.shape[2], self.images.shape[3])

    def make_model(self):
        model = Sequential()
        #get all unique labels
        nb_classes = len(np.unique(self.labels))
        for i in range(self.nb_layers):
            if i == 0:
                model.add(Conv2D(self.filters_number[i], kernel_size=(self.filters_size[i], self.filters_size[i]), activation='linear', input_shape=self.input_shape))
                model.add(LeakyReLU(alpha=0.1))
                model.add(MaxPooling2D((2, 2)))
            else:
                model.add(Conv2D(self.filters_number[i], kernel_size=(self.filters_size[i], self.filters_size[i]), activation='linear'))
                model.add(LeakyReLU(alpha=0.1))
                model.add(MaxPooling2D((2, 2)))
        model.add(Flatten())
        model.add(Dense(128, activation='linear'))
        model.add(LeakyReLU(alpha=0.1))                  
        model.add(Dense(nb_classes, activation='softmax'))
        if nb_classes > 2:
            model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adam(),metrics=['accuracy'])
        else:
            model.compile(loss=keras.losses.binary_crossentropy, optimizer=keras.optimizers.Adam(),metrics=['accuracy'])
        return model
