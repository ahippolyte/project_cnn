import os
import shutil
import pygame as pg
import numpy as np
from PIL import Image
import csv
import pandas as pd
import classes.utilities.FileExplorer as FE
from classes.shapes import *
from classes.factories.ShapeFactory import ShapeFactory
from classes.shapes.Circle import Circle
from classes.shapes.Square import Square
from classes.shapes.Triangle import Triangle 
from classes.utilities.ConfigNavigator import ConfigNavigator
import re

class DatasetGenerator:
    circles = False
    squares = False
    triangles = False
    empties = False
    number = 100
    nb_shapes = 0
    size = 256
    surface = None
    folder = None
    save = True
    shapeFactory = ShapeFactory()
    images = []
    labels = []
    csv_name = None
    
    def __init__(self, circles, squares, triangles, empties, number, size, save, folder):
        if not (circles or squares or triangles or empties):
            raise ValueError("At least one shape must be True")
        if number < 1:
            raise ValueError("Number of images must be greater than 1")
        if size < 2:
            raise ValueError("Size of images must be greater than 2")
        if not isinstance(save, bool):
            raise ValueError("Save must be a boolean")
        if not isinstance(folder, str):
            raise ValueError("Folder must be a string")
        if not re.search(r'^[^\\/?%*:|"<>\.]+$', folder) or folder == "":
            raise ValueError("Folder must be a valid folder name")

        self.circles = circles
        self.squares = squares
        self.triangles = triangles
        self.empties = empties
        self.number = number
        self.size = size
        self.folder = folder
        self.save = save
        self.find_csv_name()

        self.nb_shapes = (1 if circles else 0) + (1 if squares else 0) + (1 if triangles else 0) + (1 if empties else 0)
        self.surface = pg.Surface((self.size, self.size))
        self.surface.fill((0, 0, 0))

    def find_csv_name(self):
        fe = FE.getFolderContent(ConfigNavigator.find("folders/content"))
        max_num = 0
        if self.csv_name is None:
            for file in fe:
                if file.endswith(".csv"):
                    if file.startswith("generated"):
                        num = int(file[9:-4])
                        if num > max_num:
                            max_num = num
            self.csv_name = "generated" + str(max_num + 1) + ".csv"
            print("CSV saved as : " + self.csv_name)
            


    def get_number_of_shapes(self):
        return self.nb_shapes

    def get_number_of_images(self):
        return self.number

    def get_size(self):
        return self.size

    def get_folder(self):
        return self.folder

    def generate(self):
        self.generate_dataset_of_forms()
        self.generate_csv_from_folder()
        self.load_csv()
        if not self.save:
            self.remove_dataset_folder()
        return self.images, self.labels

    def generate_from_folder(self, folder):
        self.folder = folder
        self.generate_csv_from_folder()
        self.load_csv()
        return self.images, self.labels

    def remove_dataset_folder(self):
        for filename in os.listdir(self.folder):
            file_path = os.path.join(self.folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
        os.rmdir(self.folder)

    def generate_dataset_of_forms(self):
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)
        else:
            try :
                for filename in os.listdir(self.folder):
                    file_path = os.path.join(self.folder, filename)
                    try:
                        if os.path.isfile(file_path) or os.path.islink(file_path):
                            os.unlink(file_path)
                        elif os.path.isdir(file_path):
                            shutil.rmtree(file_path)
                    except Exception as e:
                        print('Failed to delete %s. Reason: %s' % (file_path, e))
            except:
                print("Error while deleting files in folder")

        nb_each = self.number//self.nb_shapes
        for i in range(nb_each):
            if self.circles:
                self.generate_circle(i)
            if self.squares:
                self.generate_square(i)
            if self.triangles:
                self.generate_triangle(i)
            if self.empties:
                self.generate_empty(i)
        missing = self.number - nb_each*self.nb_shapes
        if nb_each*self.nb_shapes < self.number:
            for i in range(nb_each, nb_each+missing):
                if self.circles:
                    self.generate_circle(i)
                elif self.squares:
                    self.generate_square(i)
                elif self.triangles:
                    self.generate_triangle(i)
                elif self.empties:
                    self.generate_empty(i)

    def generate_random_parameters(self):
        random_size = np.random.randint(self.size/8, (self.size/2))
        random_pos_x = np.random.randint(self.size/2 - random_size, self.size/2 + random_size + 1)
        random_pos_y = np.random.randint(self.size/2 - random_size, self.size/2 + random_size + 1)
        return random_pos_x, random_pos_y, random_size

    def generate_circle(self, i):
        random_pos_x, random_pos_y, random_size = self.generate_random_parameters()
        c = self.shapeFactory.createCircle(random_pos_x, random_pos_y, random_size, i)
        c.draw_and_save(self.surface, self.folder)
        self.surface.fill((0, 0, 0))

    def generate_square(self, i):
        random_pos_x, random_pos_y, random_size = self.generate_random_parameters()
        s = self.shapeFactory.createSquare(random_pos_x, random_pos_y, random_size, i)
        s.draw_and_save(self.surface,   self.folder)
        self.surface.fill((0, 0, 0))

    def generate_triangle(self, i):
        random_pos_x, random_pos_y, random_size = self.generate_random_parameters()
        t = self.shapeFactory.createTriangle(random_pos_x, random_pos_y, random_size, i)
        t.draw_and_save(self.surface, self.folder)
        self.surface.fill((0, 0, 0))

    def generate_empty(self, i):
        pg.image.save(self.surface, self.folder + "/empty" + str(i) + ".png")
        self.surface.fill((0, 0, 0))

    def createFileList(self, format=".png"):
        fileList = []
        labels = []
        names = []
        keywords = {"circle" : "0","triangle":"2", "square" : "1" , "empty" : "3"}
        for root, dirs, files in os.walk(self.folder, topdown=True):
                for name in files:
                    if name.endswith(format):

                        fullName = os.path.join(root, name)
                        fileList.append(fullName)
                    for keyword in keywords:
                        if keyword in name:
                            labels.append(keywords[keyword])
                        else:
                            continue
                    names.append(name)
        return fileList, labels, names
    
    def generate_csv_from_folder(self) :
        myFileList, labels, names = self.createFileList()
        FE.create_csv(myFileList, labels, self.csv_name)

    def load_csv(self) :
        path_to_csv = ConfigNavigator.find("folders/content") + self.csv_name
        # Load the CSV file
        data = pd.read_csv(path_to_csv, header=None, skiprows=1)
        classes_in_csv = []
        # Extract the classes
        if self.circles:
            classes_in_csv.append(0)
        if self.squares:
            classes_in_csv.append(1)
        if self.triangles:
            classes_in_csv.append(2)
        if self.empties:
            classes_in_csv.append(3)
        # Extract the labels
        labels = data.iloc[:, -1].to_numpy()
        new_labels = [i for i in range(self.nb_shapes)]

        for x in range(len(labels)):
            for i in range(len(classes_in_csv)):
                if labels[x] == classes_in_csv[i]:
                    labels[x] = new_labels[i]
        # Extract the images
        images = data.iloc[:, :-1].to_numpy().reshape(-1, self.size, self.size, 1)
        self.images = images
        self.labels = labels
