import pandas as pd
import numpy as np

class CsvReader:
    path = None
    images, labels = None, None
    image_size = None

    def __init__(self, path):
        self.path = path

    def read(self):
        # Load the CSV file
        data = pd.read_csv(self.path)
        # Find the label column based on the first row of the CSV file
        label_column = None
        for col in data.columns:
            if col.lower() == 'labels' or col.lower() == 'label':
                label_column = col
                break
        if label_column is None:
            raise ValueError('Label column not found in CSV file')
        # Extract the image size from the number of columns
        self.image_size = int(np.sqrt(data.shape[1] - 1))
        # Extract the labels from the label column
        labels = data[label_column].to_numpy()
        # Extract the image data
        image_columns = [col for col in data.columns if col != label_column]
        image_data = data[image_columns].to_numpy()
        # Reshape the image data according to the image size
        try : 
            images = image_data.reshape(-1, self.image_size, self.image_size, 1)
        except ValueError:
            raise ValueError('Image size is not a square number')
        # Store the images and labels in the class variables
        self.images, self.labels = images, labels
        return images, labels

