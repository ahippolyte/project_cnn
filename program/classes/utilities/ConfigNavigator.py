import json

class ConfigNavigator:
    def find(path):
        # Upercase all letters of the path
        path = path.upper()
        path = path.split("/")
        config = None
        with open('config/config.json', 'r') as fichier_json:
            config = json.load(fichier_json)
        for i in range(len(path)):
            config = config[path[i]]
        return config