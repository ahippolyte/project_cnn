class Proxy:
    type = None
    item = None
    name = None

    def __init__(self, name, type, item):
        if(not name or not item):
            raise ValueError("Proxy name and item cannot be empty")
        self.name = name
        self.type = type
        self.item = item

    def get_name(self):
        return self.name

    def get_type(self):
        return self.type
    
    def get_item(self):
        return self.item