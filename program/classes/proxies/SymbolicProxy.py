from classes.proxies.Proxy import Proxy
from classes.utilities.ConfigNavigator import ConfigNavigator
import json

class SymbolicProxy(Proxy):
    def __init__(self, name, item):
        super().__init__(name, "symbolic", item)
    
    def save(self):
        file = open(ConfigNavigator.find("folders/content") + self.name + ".proxy", "w")
        file.write(self.item)
        file.close()