from classes.proxies.Proxy import Proxy

class FileProxy(Proxy):
    def __init__(self, name, item):
        super().__init__(name, "file", item)