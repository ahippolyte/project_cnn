from classes.proxies.Proxy import Proxy

class TensorflowProxy(Proxy):
    def __init__(self, name, item):
        super().__init__(name, "tensorflow", item)