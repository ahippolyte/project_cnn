import unittest
import os
import sys
import shutil

sys.path.append('..')
from classes.utilities.FileExplorer import *

class TestFileExploreur(unittest.TestCase):

    def test_getFileContent_positive(self):
        test_file = open('test_file.txt', 'w')
        validation_string = 'This is a test String ! \n'
        test_file.write(validation_string)
        test_file.close()
        tested_string = getFileContent('test_file.txt')
        os.remove('test_file.txt')
        self.assertEqual(tested_string, validation_string)

    def test_getFileContent_negative(self):
        if os.path.isfile('test_file.txt'):
            os.remove('test_file.txt')
        self.assertRaises(FileNotFoundError, getFileContent, 'test_file.txt')

    def test_getFolderContent_positive(self):
        os.mkdir('./test_folder')

        test_file = open('test_folder/test_file1.txt', 'w')
        validation_string = 'This is a test String ! \n'
        test_file.write(validation_string)
        test_file.close()

        test_file = open('test_folder/test_file2.txt', 'w')
        test_file.write(validation_string)
        test_file.close()

        test_file = open('test_folder/test_file3.txt', 'w')
        test_file.write(validation_string)
        test_file.close()

        tested_list = getFolderContent('./test_folder')
        validation_list = ['test_file1.txt', 'test_file2.txt', 'test_file3.txt']

        shutil.rmtree('./test_folder')

        self.assertCountEqual(tested_list, validation_list)

    def test_getFolderContent_positive_2(self):
        if os.path.isdir('./test_folder'):
          shutil.rmtree('./test_folder')

        getFolderContent('./test_folder')
        tested_bool = os.path.isdir('./test_folder')
        shutil.rmtree('./test_folder')

        self.assertEqual(tested_bool, True)
