import unittest
import sys

sys.path.append('..')

from classes.utilities.DatasetGenerator import DatasetGenerator

class TestDatasetGenerator(unittest.TestCase):

    def test_dataset_generator_positive(self):
        dg = DatasetGenerator(True, True, True, True, 100, 256, False, "bin")
        result_get_number_of_shapes = dg.get_number_of_shapes()
        result_get_number_of_images = dg.get_number_of_images()
        result_get_size = dg.get_size()
        result_get_folder = dg.get_folder()
        self.assertEqual(result_get_number_of_shapes, 4)
        self.assertEqual(result_get_number_of_images, 100)
        self.assertEqual(result_get_size, 256)
        self.assertEqual(result_get_folder, "bin")

        images, labels = dg.generate()
        self.assertEqual(images.shape, (100, 256, 256, 1))
        self.assertEqual(labels.shape, (100, ))

    def test_dataset_generator_negative(self):
        with self.assertRaises(ValueError) as context:
            dg = DatasetGenerator(True, True, True, True, -1, 256, False, "bin")
        self.assertEqual('Number of images must be greater than 1', str(context.exception))

        with self.assertRaises(ValueError) as context:
            dg = DatasetGenerator(True, True, True, True, 100, -1, False, "bin")
        self.assertEqual('Size of images must be greater than 2', str(context.exception))

        with self.assertRaises(ValueError) as context:
            dg = DatasetGenerator(True, True, True, True, 100, 256, False, "")
        self.assertEqual('Folder must be a valid folder name', str(context.exception))

        with self.assertRaises(ValueError) as context:
            dg = DatasetGenerator(True, True, True, True, 100, 256, False, "bin/")
        self.assertEqual('Folder must be a valid folder name', str(context.exception))
        
        with self.assertRaises(ValueError) as context:
            dg = DatasetGenerator(False, False, False, False, 100, 256, False, "bin")
        self.assertEqual('At least one shape must be True', str(context.exception))

    def test_dataset_generator_at_limit(self):
        dg = DatasetGenerator(True, True, True, True, 1, 2, False, "bin")
        result_get_number_of_shapes = dg.get_number_of_shapes()
        result_get_number_of_images = dg.get_number_of_images()
        result_get_size = dg.get_size()
        result_get_folder = dg.get_folder()
        self.assertEqual(result_get_number_of_shapes, 4)
        self.assertEqual(result_get_number_of_images, 1)
        self.assertEqual(result_get_size, 2)
        self.assertEqual(result_get_folder, "bin")

        images, labels = dg.generate()
        self.assertEqual(images.shape, (1, 2, 2, 1))
        self.assertEqual(labels.shape, (1, ))
        

