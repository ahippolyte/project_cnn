import unittest
import sys

sys.path.append('..')

from classes.utilities.CsvReader import CsvReader

class TestCsvReader(unittest.TestCase):

    def test_csv_reader_positive(self):
        cr = CsvReader("test.csv")
        images, labels = cr.read()
        self.assertEqual(images.shape, (3, 3, 3, 1))
        self.assertEqual(labels.shape, (3, ))

    def test_csv_reader_negative(self):
        with self.assertRaises(ValueError) as context:
            cr = CsvReader("bad_test_label.csv")
            cr.read()
        self.assertEqual('Label column not found in CSV file', str(context.exception))

        with self.assertRaises(ValueError) as context:
            cr = CsvReader("bad_test_size.csv")
            cr.read()
        self.assertEqual('Image size is not a square number', str(context.exception))


        

