import unittest

import sys
sys.path.append('..')
from classes.shapes.Circle import *
from classes.shapes.Square import *
from classes.shapes.Triangle import *

class TestCircle(unittest.TestCase):

    def test_circle_positive(self):
        c1 = Circle(5, 6, 10, 2)
        c2 = Circle(-1, -5, 1, 1)
        self.assertEqual(c1.get_x(), 5)
        self.assertEqual(c1.get_y(), 6)
        self.assertEqual(c1.get_size(), 10)
        self.assertEqual(c1.get_count(), 2)
        self.assertIsInstance(c1, Shape)
        self.assertEqual(c2.get_x(), -1)
        self.assertEqual(c2.get_y(), -5)
        self.assertEqual(c2.get_size(), 1)
        self.assertEqual(c2.get_count(), 1)
        self.assertIsInstance(c2, Shape)

    def test_circle_negative(self):
        with self.assertRaises(ValueError) as context:
            c = Circle(0, 0, -1, 0)
        self.assertEqual('Size must be greater than 0', str(context.exception))
        with self.assertRaises(ValueError) as context:
            c = Circle(0, 0, 0, -1)
        self.assertEqual('Count must be greater than 0', str(context.exception))

    def test_circle_limit(self):
        c = Circle(0, 0, 0, 0)
        self.assertEqual(c.get_x(), 0)
        self.assertEqual(c.get_y(), 0)
        self.assertEqual(c.get_size(), 0)
        self.assertEqual(c.get_count(), 0)
        self.assertIsInstance(c, Shape)
        

class TestSquare(unittest.TestCase):

    def test_square_positive(self):
        s1 = Square(5, 6, 10, 2)
        s2 = Square(-1, -5, 1, 1)
        self.assertEqual(s1.get_x(), 5)
        self.assertEqual(s1.get_y(), 6)
        self.assertEqual(s1.get_size(), 10)
        self.assertEqual(s1.get_count(), 2)
        self.assertIsInstance(s1, Shape)
        self.assertEqual(s2.get_x(), -1)
        self.assertEqual(s2.get_y(), -5)
        self.assertEqual(s2.get_size(), 1)
        self.assertEqual(s2.get_count(), 1)
        self.assertIsInstance(s2, Shape)

    def test_square_negative(self):
        with self.assertRaises(ValueError) as context:
            s = Square(0, 0, -1, 0)
        self.assertEqual('Size must be greater than 0', str(context.exception))
        with self.assertRaises(ValueError) as context:
            s = Square(0, 0, 0, -1)
        self.assertEqual('Count must be greater than 0', str(context.exception))

    def test_square_limit(self):
        c = Square(0, 0, 0, 0)
        self.assertEqual(c.get_x(), 0)
        self.assertEqual(c.get_y(), 0)
        self.assertEqual(c.get_size(), 0)
        self.assertEqual(c.get_count(), 0)
        self.assertIsInstance(c, Shape)


class TestTriangle(unittest.TestCase):
    def test_triangle_positive(self):
        t1 = Triangle(5, 6, 10, 2)
        t2 = Triangle(-1, -5, 1, 1)
        self.assertEqual(t1.get_x(), 5)
        self.assertEqual(t1.get_y(), 6)
        self.assertEqual(t1.get_size(), 10)
        self.assertEqual(t1.get_count(), 2)
        self.assertIsInstance(t1, Shape)
        self.assertEqual(t2.get_x(), -1)
        self.assertEqual(t2.get_y(), -5)
        self.assertEqual(t2.get_size(), 1)
        self.assertEqual(t2.get_count(), 1)
        self.assertIsInstance(t2, Shape)

    def test_triangle_negative(self):
        with self.assertRaises(ValueError) as context:
            s = Triangle(0, 0, -1, 0)
        self.assertEqual('Size must be greater than 0', str(context.exception))
        with self.assertRaises(ValueError) as context:
            s = Triangle(0, 0, 0, -1)
        self.assertEqual('Count must be greater than 0', str(context.exception))


    def test_triangle_limit(self):
        c = Triangle(0, 0, 0, 0)
        self.assertEqual(c.get_x(), 0)
        self.assertEqual(c.get_y(), 0)
        self.assertEqual(c.get_size(), 0)
        self.assertEqual(c.get_count(), 0)
        self.assertIsInstance(c, Shape)