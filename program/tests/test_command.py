import unittest

import sys
sys.path.append('..')
from classes.commands.command.Command import *
from classes.commands.CommandList import *

class TestCommand(unittest.TestCase):
    def test_command_positive(self):
        command = Command("Test description", "--test", "-t")
        result_get_description = command.get_description()
        result_get_command = command.get_command()
        result_get_shortcut = command.get_shortcut()
        self.assertEqual(result_get_description, "Test description")
        self.assertEqual(result_get_command, "--test")
        self.assertEqual(result_get_shortcut, "-t")

    def test_command_negative(self):
        with self.assertRaises(ValueError) as context1:
            command = Command("", "--test", "-t")
        with self.assertRaises(ValueError) as context2:
            command = Command("Test description", "", "-t")
        with self.assertRaises(ValueError) as context3:
            command = Command("Test description", "--test", "")
        self.assertEqual('Command attributes cannot be empty', str(context1.exception))
        self.assertEqual('Command attributes cannot be empty', str(context2.exception))
        self.assertEqual('Command attributes cannot be empty', str(context3.exception))

class TestCommandList(unittest.TestCase):
    def test_command_list_positive(self):
        commandList = CommandList()
        command1 = Command("Command 1", "--one", "-1")
        command2 = Command("Command 2", "--two", "-2")
        self.assertEqual(commandList.get_commands(), [])
        commandList.add(command1)
        self.assertEqual(commandList.get_commands(), [command1])
        commandList.add(command2)
        self.assertEqual(commandList.get_commands(), [command1, command2])
        commandList.remove(command2)
        self.assertEqual(commandList.get_command("--one"), command1)
        self.assertEqual(commandList.get_command("-1"), command1)
        self.assertIsNone(commandList.get_command("-x"))
