import unittest
import sys

from mock import patch

sys.path.append('..')

from classes.utilities.TensorflowLoader import TensorflowLoader
from classes.ui.TensorflowLoaderUI import TensorflowLoaderUI

class TestTensorflowLoader(unittest.TestCase):


    @patch('classes.ui.TensorflowLoaderUI.TensorflowLoaderUI.ask_part_of_dataset', return_value=100)
    def test_tensorflow_loader_positive(self, mock_ask_part_of_dataset):
        tl = TensorflowLoader("mnist")

        images, labels = tl.start()
        self.assertEqual(images.shape, (60000, 28, 28, 1))
        self.assertEqual(labels.shape, (60000, ))


    @patch('classes.ui.TensorflowLoaderUI.TensorflowLoaderUI.ask_part_of_dataset', return_value=100)
    def test_tensorflow_loader_negative(self, mock_ask_part_of_dataset):
        with self.assertRaises(ValueError) as context:
            tl = TensorflowLoader("prout")
            tl.start()
        self.assertEqual('Dataset name must be a valid dataset name', str(context.exception))
