import unittest

import sys
sys.path.append('..')
from classes.factories.ShapeFactory import *

class TestShapeFactory(unittest.TestCase):
    factory = ShapeFactory()
    def test_circle(self):
        circle = self.factory.createCircle(5, 6, 10, 2)
        result_get_x = circle.get_x()
        result_get_y = circle.get_y()
        result_get_size = circle.get_size()
        result_get_count = circle.get_count()
        self.assertEqual(result_get_x, 5)
        self.assertEqual(result_get_y, 6)
        self.assertEqual(result_get_size, 10)
        self.assertEqual(result_get_count, 2)
        self.assertIsInstance(circle, Circle)

    def test_square(self):
        square = self.factory.createSquare(5, 6, 10, 2)
        result_get_x = square.get_x()
        result_get_y = square.get_y()
        result_get_size = square.get_size()
        result_get_count = square.get_count()
        self.assertEqual(result_get_x, 5)
        self.assertEqual(result_get_y, 6)
        self.assertEqual(result_get_size, 10)
        self.assertEqual(result_get_count, 2)
        self.assertIsInstance(square, Square)

    def test_triangle(self):
        triangle = self.factory.createTriangle(5, 6, 10, 2)
        result_get_x = triangle.get_x()
        result_get_y = triangle.get_y()
        result_get_size = triangle.get_size()
        result_get_count = triangle.get_count()
        self.assertEqual(result_get_x, 5)
        self.assertEqual(result_get_y, 6)
        self.assertEqual(result_get_size, 10)
        self.assertEqual(result_get_count, 2)
        self.assertIsInstance(triangle, Triangle)