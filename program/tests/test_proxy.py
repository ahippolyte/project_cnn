import unittest

import sys
sys.path.append('..')
from classes.proxies.FileProxy import *
from classes.proxies.SymbolicProxy import *
from classes.proxies.TensorflowProxy import *

class TestFileProxy(unittest.TestCase):

    def test_file_proxy_positive(self):
        p = FileProxy("file_proxy", "path/to/destination")
        result_get_name = p.get_name()
        result_get_item = p.get_item()
        result_get_type = p.get_type()
        self.assertEqual(result_get_name, "file_proxy")
        self.assertEqual(result_get_item, "path/to/destination")
        self.assertEqual(result_get_type, "file")
        self.assertIsInstance(p, Proxy)

    def test_file_proxy_negative(self):
        with self.assertRaises(ValueError) as context:
            p = FileProxy("", "")
        self.assertEqual('Proxy name and item cannot be empty', str(context.exception))
        

class TestSymbolicProxy(unittest.TestCase):

    def test_symbolic_proxy_positive(self):
        p = SymbolicProxy("symbolic_proxy", "path/to/destination")
        result_get_name = p.get_name()
        result_get_item = p.get_item()
        result_get_type = p.get_type()
        self.assertEqual(result_get_name, "symbolic_proxy")
        self.assertEqual(result_get_item, "path/to/destination")
        self.assertEqual(result_get_type, "symbolic")
        self.assertIsInstance(p, Proxy)

    def test_symbolic_proxy_negative(self):
        with self.assertRaises(ValueError) as context:
            p = SymbolicProxy("", "")
        self.assertEqual('Proxy name and item cannot be empty', str(context.exception))


class TestTensorflowProxy(unittest.TestCase):

    def test_tensorflow_proxy_positive(self):
        p = TensorflowProxy("tensorflow_proxy", "path/to/destination")
        result_get_name = p.get_name()
        result_get_item = p.get_item()
        result_get_type = p.get_type()
        self.assertEqual(result_get_name, "tensorflow_proxy")
        self.assertEqual(result_get_item, "path/to/destination")
        self.assertEqual(result_get_type, "tensorflow")
        self.assertIsInstance(p, Proxy)

    def test_tensorflow_proxy_negative(self):
        with self.assertRaises(ValueError) as context:
            p = TensorflowProxy("", "")
        self.assertEqual('Proxy name and item cannot be empty', str(context.exception))