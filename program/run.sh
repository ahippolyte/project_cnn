#!/bin/sh
clear
# check if $1 = '--install' or '-i'
if [ "$1" = "--install" ] || [ "$1" = "-i" ]; then
    sh ./scripts/build.sh
    sh ./scripts/install.sh
    "$1" = "$2"
    "$2" = "$3"
else
    if [ -f "./scripts/.already-started" ]; then
        echo "Skipping dependencies requirments. If you want to install them, run the program with the --install or -i flag."
    else
        echo "This is the first time you run this program. Do you want to install dependencies requirments ? (yes/no)"
        read answer
        answer=$(echo "$answer" | tr '[:upper:]' '[:lower:]')
        if [[ "$answer" == "yes" ]]; then
            sh ./scripts/build.sh
            sh ./scripts/install.sh
        elif [[ "$answer" == "y" ]]; then
            sh ./scripts/build.sh
            sh ./scripts/install.sh
        elif [[ "$answer" == "no" ]]; then
            echo "Skipping dependencies requirments. If you want to install them, run the program with the --install or -i flag." 
            touch ./scripts/.already-started
        elif [[ "$answer" == "n" ]]; then
            echo "Skipping dependencies requirments. If you want to install them, run the program with the --install or -i flag."
            touch ./scripts/.already-started
        else
            echo "Invalid answer. Skipping dependencies requirments. If you want to install them, run the program with the --install or -i flag."
        fi
    fi
fi
echo "Running program..."
clear
python3 ./app.py $1 $2
echo "Program ended."